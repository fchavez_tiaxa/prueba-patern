package cl.mobid.services.adapterconsalud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdapterConsaludApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdapterConsaludApplication.class, args);
    }

}
